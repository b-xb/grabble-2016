package com.example.ben.newtest;

import java.util.WeakHashMap;

public class DataPreload {


    public static WeakHashMap<String, Integer> loadLetterTiles(WeakHashMap<String, Integer> letterTiles){

        letterTiles.put("A",R.drawable.a48x48);
        letterTiles.put("B",R.drawable.b48x48);
        letterTiles.put("C",R.drawable.c48x48);
        letterTiles.put("D",R.drawable.d48x48);
        letterTiles.put("E",R.drawable.e48x48);
        letterTiles.put("F",R.drawable.f48x48);
        letterTiles.put("G",R.drawable.g48x48);
        letterTiles.put("H",R.drawable.h48x48);
        letterTiles.put("I",R.drawable.i48x48);
        letterTiles.put("J",R.drawable.j48x48);
        letterTiles.put("K",R.drawable.k48x48);
        letterTiles.put("L",R.drawable.l48x48);
        letterTiles.put("M",R.drawable.m48x48);
        letterTiles.put("N",R.drawable.n48x48);
        letterTiles.put("O",R.drawable.o48x48);
        letterTiles.put("P",R.drawable.p48x48);
        letterTiles.put("Q",R.drawable.q48x48);
        letterTiles.put("R",R.drawable.r48x48);
        letterTiles.put("S",R.drawable.s48x48);
        letterTiles.put("T",R.drawable.t48x48);
        letterTiles.put("U",R.drawable.u48x48);
        letterTiles.put("V",R.drawable.v48x48);
        letterTiles.put("W",R.drawable.w48x48);
        letterTiles.put("X",R.drawable.x48x48);
        letterTiles.put("Y",R.drawable.y48x48);
        letterTiles.put("Z",R.drawable.z48x48);

        return letterTiles;
    }


    public static WeakHashMap<String, Integer> loadLetterScores(WeakHashMap<String, Integer> letterScores) {


        letterScores.put("A",3);
        letterScores.put("B",20);
        letterScores.put("C",13);
        letterScores.put("D",10);
        letterScores.put("E",1);
        letterScores.put("F",15);
        letterScores.put("G",18);
        letterScores.put("H",9);
        letterScores.put("I",5);
        letterScores.put("J",25);
        letterScores.put("K",22);
        letterScores.put("L",11);
        letterScores.put("M",14);
        letterScores.put("N",6);
        letterScores.put("O",4);
        letterScores.put("P",19);
        letterScores.put("Q",24);
        letterScores.put("R",8);
        letterScores.put("S",7);
        letterScores.put("T",2);
        letterScores.put("U",12);
        letterScores.put("V",21);
        letterScores.put("W",17);
        letterScores.put("X",23);
        letterScores.put("Y",16);
        letterScores.put("Z",26);

        return letterScores;
    }


}