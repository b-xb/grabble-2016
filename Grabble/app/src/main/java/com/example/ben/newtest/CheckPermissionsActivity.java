package com.example.ben.newtest;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class CheckPermissionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_permissions);

        runPermissionsCheck();
    }


    public void runPermissionsCheck() {
        //Check for GPS Permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("CheckPermissionsAct", "NO GPS PERMISSION");

            //Request GPS permission
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);

        } else {
            Log.e("CheckPermissionsAct", "WE HAVE GPS PERMISSION");

            loadMainMenu();
        }
    }

    //called by requestPermissions with result of permission check
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("CheckPermissionsAct", "GPS PERMISSION GRANTED");

                    loadMainMenu();

                } else {

                    Log.e("CheckPermissionsAct", "GPS PERMISSION DENIED");

                    new AlertDialog.Builder(this)
                            .setTitle("Permissions Needed")
                            .setMessage("You cannot play this game without allowing access to your device's location.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    runPermissionsCheck();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
                return;
            }
        }
    }

    private void loadMainMenu() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

}
