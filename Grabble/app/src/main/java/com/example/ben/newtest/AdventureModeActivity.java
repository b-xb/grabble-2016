package com.example.ben.newtest;

import android.*;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.WeakHashMap;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;

public class AdventureModeActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleMap.OnMarkerClickListener, TabHost.OnTabChangeListener {

    private GoogleMap mMap;
    private MapView mapView;
    private Document mapData;
    private GrabbleDbHelper grabbleDbHelper;

    private static WeakHashMap<String, Integer> letterTiles = new WeakHashMap<String, Integer>();
    private static WeakHashMap<String, Integer> letterScores = new WeakHashMap<String, Integer>();
    private static WeakHashMap<String, Marker> letterMarkers = new WeakHashMap<String, Marker>();

    private LocationManager locationManager;
    private Location gpsLocation;
    private Marker gpsMarker = null;
    private LetterCollectionAdapter letterCollectionAdapter;

    SQLiteDatabase readDb;
    SQLiteDatabase writeDb;

    Long todayDate;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adventure_mode);

        initialiseData();
        setUpTabs();
        initialiseMap(savedInstanceState);
    }

    private void initialiseData() {
        sharedPreferences = getSharedPreferences(GrabbleContract.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        DataPreload.loadLetterTiles(letterTiles);
        DataPreload.loadLetterScores(letterScores);
        grabbleDbHelper = new GrabbleDbHelper(this);
        readDb = grabbleDbHelper.getReadableDatabase();
        writeDb = grabbleDbHelper.getWritableDatabase();
        letterCollectionAdapter = new LetterCollectionAdapter(this,letterTiles,readDb);
        loadLetterCollection();
    }


    private void setUpTabs() {

        //Set up Tabs
        TabHost host = (TabHost) findViewById(R.id.tabhost);
        host.setup();

        //Tab 1 - Map Tab
        TabHost.TabSpec spec = host.newTabSpec("Map");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Map");
        host.addTab(spec);

        //Tab 2 - Word Spelling Tab
        spec = host.newTabSpec("Spell");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Spell");
        host.addTab(spec);

        host.setOnTabChangedListener(this);

    }

    private void initialiseMap(Bundle savedInstanceState) {
        //Initialise Map
        mapView = (MapView) findViewById(R.id.gamemap);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);  //calls onMapReady when map is loaded
    }

    //
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);

        //get phone width and height for use in initialising map
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.05); // offset from edges of the map 5% of screen

        //initialise view to be the whole playfield size
        LatLng playfieldNE = new LatLng(55.946233, -3.184319);
        LatLng playfieldSW = new LatLng(55.942617, -3.192473);
        LatLngBounds playfieldbounds = new LatLngBounds(playfieldSW, playfieldNE);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(playfieldbounds, width, height, padding);
        mMap.moveCamera(cu);

        //Load markers
        loadLetterMarkers();
    }

    private void loadLetterMarkers() {
        // Check Date and Time against Date and Time for current map
        // If Current Date and Time still same day as current map, load from existing data
        // Otherwise load new map

        Long todayDateTime = new Date().getTime();
        todayDate = new Date(todayDateTime - todayDateTime % (24 * 60 * 60 * 1000)).getTime();



        Long storedDate = sharedPreferences.getLong(GrabbleContract.CURRENT_MAP_DATE,0);

        if (storedDate < todayDate) {

            Calendar calendar = new GregorianCalendar();
            int day = calendar.get(Calendar.DAY_OF_WEEK);

            String dayAsString = calendar.getDisplayName( Calendar.DAY_OF_WEEK ,Calendar.LONG, Locale.getDefault());

            String dateAsString = new SimpleDateFormat("EEEE dd/MM/yyyy").format(todayDate);


            Toast.makeText(getBaseContext(), "Loading New Map:\n"+dateAsString, Toast.LENGTH_LONG).show();

            new DownloadRemoteMap().execute(GrabbleContract.MAP_URIS[day]);

            updateScoreBoard(fetchHighScore());
        } else {
            updateScoreBoard(fetchHighScore());
            addLetterMarkers();
        }
    }

    private class DownloadRemoteMap extends AsyncTask<String, Void, String> {
        String downloadSuccessful = "false";

        protected String doInBackground(String... url) {

            try {
                mapData = Jsoup.connect(url[0]).ignoreContentType(true).get();
                downloadSuccessful = "true";
            } catch (IOException e) {
                e.printStackTrace();
                downloadSuccessful = "false";
            }

            return downloadSuccessful;
        }

        protected void onPostExecute(String url) {
            if (downloadSuccessful == "true") {
                storeLetterMarkers();
            } else {
                Toast.makeText(getBaseContext(), "Trouble with Internet Connection.\nUnable to load new Map.", Toast.LENGTH_LONG).show();
            }
            System.out.println("Map downloaded?: " + downloadSuccessful);
        }
    }

    private void storeLetterMarkers() {

        // Delete Previous Markers
        writeDb.delete(GrabbleContract.Markers.TABLE_NAME, null, null);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(GrabbleContract.CURRENT_MAP_DATE, todayDate);
        editor.commit();


        for (Element e : mapData.select("Placemark")) {

            String coordinateString = e.select("Point").select("coordinates").text();
            String[] coordinates = coordinateString.split(",");

            String id = e.select("name").text();
            String letter = e.select("description").text();
            Double latitude = Double.parseDouble(coordinates[1]);
            Double longitude = Double.parseDouble(coordinates[0]);

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(GrabbleContract.Markers.COLUMN_NAME_ID, id);
            values.put(GrabbleContract.Markers.COLUMN_NAME_LETTER, letter);
            values.put(GrabbleContract.Markers.COLUMN_NAME_LATITUDE, latitude);
            values.put(GrabbleContract.Markers.COLUMN_NAME_LONGITUDE, longitude);
            values.put(GrabbleContract.Markers.COLUMN_NAME_ADVENTURE_MODE_STATE, GrabbleContract.MARKER_UP_FOR_GRABS);
            values.put(GrabbleContract.Markers.COLUMN_NAME_CHALLENGE_MODE_STATE, GrabbleContract.MARKER_UP_FOR_GRABS);
            values.put(GrabbleContract.Markers.COLUMN_NAME_TIMER_MODE_STATE, GrabbleContract.MARKER_UP_FOR_GRABS);

            // Insert the new row, returning the primary key value of the new row
            writeDb.insert(GrabbleContract.Markers.TABLE_NAME, null, values);


        }

        addLetterMarkers();
    }

    private void addLetterMarkers() {

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                GrabbleContract.Markers.COLUMN_NAME_ID,
                GrabbleContract.Markers.COLUMN_NAME_LETTER,
                GrabbleContract.Markers.COLUMN_NAME_LATITUDE,
                GrabbleContract.Markers.COLUMN_NAME_LONGITUDE,
                GrabbleContract.Markers.COLUMN_NAME_ADVENTURE_MODE_STATE
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = GrabbleContract.Markers.COLUMN_NAME_ADVENTURE_MODE_STATE + " = ?";
        String[] selectionArgs = {(GrabbleContract.MARKER_UP_FOR_GRABS.toString())};

        Cursor cursor = readDb.query(
                GrabbleContract.Markers.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort order
        );

        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_ID));
            String letterValue = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_LETTER));
            Double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_LATITUDE));
            Double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_LONGITUDE));


            LatLng letterLocation = new LatLng(latitude, longitude);

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(letterLocation)
                    .title(id + ": " + letterValue)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));

            marker.setTag(id);
            letterMarkers.put(id,marker);
        }
        cursor.close();

        loadGPSMarker();
    }


    private void loadGPSMarker() {


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        try {
            gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } catch (SecurityException e) {
            //Check for GPS Permission
            runPermissionsCheck();
        }

        if (gpsLocation != null) {
            String msg = "New Latitude: " + gpsLocation.getLatitude()
                    + "\nNew Longitude: " + gpsLocation.getLongitude();

            Log.e("AdventureModeActivity", msg);

            setGpsMarker();

        } else {
            Log.e("AdventureModeActivity", "Unable to get last location!");
        }

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, this);
        } catch (SecurityException e) {
            //Check for GPS Permission
            runPermissionsCheck();
        }

    }

    @Override
    public void onLocationChanged(Location location) {

        gpsLocation = location;

        String msg = "New Latitude: " + location.getLatitude()
                + "\nNew Longitude: " + location.getLongitude();

        //Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
        Log.e("AdventureModeActivity", "onLocationChanged is called");

        setGpsMarker();

    }

    private void setGpsMarker(){


        String msg = "New Latitude: " + gpsLocation.getLatitude()
                + "\nNew Longitude: " + gpsLocation.getLongitude();

        Log.e("setGpsMarker", msg);

        LatLng gpsLatLng = new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude());

        Log.e("setGpsMarker", "LatLng: "+ gpsLatLng.toString());

        if (gpsMarker == null) {
            createGpsMarker(gpsLatLng);
        }

        gpsMarker.setPosition(gpsLatLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(gpsLatLng));

        updateVisibleLetters(gpsLatLng);

    }

    public void createGpsMarker(LatLng gpsLatLng){
        gpsMarker = mMap.addMarker(new MarkerOptions().position(gpsLatLng).title("User"));
    }


    private void updateVisibleLetters(LatLng gpsLatLng) {
        double latitudeUpperBound = gpsLocation.getLatitude() + .0006;
        double latitudeLowerBound = gpsLocation.getLatitude() - .0006;
        double longitudeUpperBound = gpsLocation.getLongitude() + 0.0012;
        double longitudeLowerBound = gpsLocation.getLongitude() - 0.0012;


        for (WeakHashMap.Entry<String, Marker> entry : letterMarkers.entrySet()) {
            String id = entry.getKey();
            Marker letterMarker = entry.getValue();

            LatLng letterPosition = letterMarker.getPosition();
            if ((letterPosition.longitude > longitudeLowerBound) &&
                    (letterPosition.longitude < longitudeUpperBound) &&
                    (letterPosition.latitude > latitudeLowerBound) &&
                    (letterPosition.latitude < latitudeUpperBound)) {

                // Swapping the lines below shows the area checked for letters
                letterMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.reddot));

                if (computeDistanceBetween(gpsLatLng, letterMarker.getPosition()) <= 50) {

                    letterMarker.setIcon(BitmapDescriptorFactory.fromResource(letterTiles.get(fetchLetter(id))));

                }

            } else {
                letterMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.reddot));
            }
        }
    }


    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    public void runPermissionsCheck() {
        //Check for GPS Permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("AdventureModeAct", "NO GPS PERMISSION");

            //Request GPS permission
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 123);

        } else {
            Log.e("AdventureModeAct", "WE HAVE GPS PERMISSION");
        }
    }

    //called by requestPermissions with result of permission check
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("AdventureModeAct", "GPS PERMISSION GRANTED");

                } else {

                    Log.e("AdventureModeAct", "GPS PERMISSION DENIED");

                    new AlertDialog.Builder(this)
                            .setTitle("Permissions Needed")
                            .setMessage("You cannot play this game without allowing access to your device's location.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    runPermissionsCheck();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            }
        }
    }



    @Override
    public boolean onMarkerClick(Marker marker) {

        if (letterCollectionCount() < 50) {

            if ((gpsMarker != null) &&
                    (marker.getTag() instanceof String) &&
                    marker.getTag().toString().startsWith("Point ")) {


                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.UK);

                double d = computeDistanceBetween(gpsMarker.getPosition(), marker.getPosition());

                if (d < 50) {
                    String letter = fetchLetter(marker.getTag().toString());

                    String output = formatter.format("%s, %.2f m away", letter, d).toString();

                    Log.e("AdventureModeActivity", "Letter " + letter + " selected. " + output );

                    addLetterToCollection(marker);

                } else {
                    String output = formatter.format("Too far away! (%.2f m)", d).toString();
                    Log.e("AdventureModeActivity", "Point Too Far Away! "+ output);
                }
            }
        } else {
            Toast.makeText(getBaseContext(), "Your Letter Collection is Full!", Toast.LENGTH_SHORT).show();
            Log.e("AdventureModeActivity", "Letter Collection Full!");
        }

        return true;


    }

    private String fetchLetter(String id){
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.Markers.COLUMN_NAME_LETTER };

        // Filter results WHERE "title" = 'My Title'
        String selection = GrabbleContract.Markers.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {(id)};

        Cursor cursor = readDb.query(
                GrabbleContract.Markers.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort order
        );

        String letterValue = null;
        while (cursor.moveToNext()) {
            letterValue = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_LETTER));
        }
        cursor.close();

        Log.e("fetchletterResult",letterValue);
        return letterValue;
    }


    private Integer letterCollectionCount(){
        String[] projection = { GrabbleContract.Markers.COLUMN_NAME_LETTER };

        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort order
        );

        return cursor.getCount();
    }

    private void addLetterToCollection(Marker marker){

        String id = marker.getTag().toString();
        String letter = fetchLetter(id);

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER, letter);

        // Insert the new row, returning the primary key value of the new row
        long rowId = writeDb.insert(GrabbleContract.AdventureModeCollection.TABLE_NAME, null, values);

        Log.e("AdventureMode","add letter "+letter+"to row id"+rowId);


        // New value for one column
        values = new ContentValues();
        values.put(GrabbleContract.Markers.COLUMN_NAME_ADVENTURE_MODE_STATE, GrabbleContract.MARKER_GRABBED);

        // Which row to update, based on the title
        String selection = GrabbleContract.Markers.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { (id) };

        readDb.update(
                GrabbleContract.Markers.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        marker.remove();
        letterMarkers.remove(id);
    }

    private void removeLetterFromCollection(int position){

        Log.e("AdventureMode","removeLetterFromCollection called");

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID };

        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,       // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,                                     // don't sort order
                position+",1"
        );

        Long id = null;
        while (cursor.moveToNext()) {
            id = cursor.getLong(cursor.getColumnIndexOrThrow(GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID));
        }
        cursor.close();

        Log.e("AdventureMode","Removing letter id: "+id.toString());

        // Which row to update, based on the title
        String selection = GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { (id.toString()) };

        int count = writeDb.delete(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,
                selection,
                selectionArgs);

        Log.e("AdventureModeActivity",count+" letters removed from collection");

        letterCollectionAdapter.notifyDataSetChanged();
    }


    @Override
    public void onTabChanged(String tabId) {
        Log.e("OnTabChanged","tabId: "+tabId);

        if (tabId == "Spell") {
            letterCollectionAdapter.notifyDataSetChanged();
        }
    }

    private void loadLetterCollection(){
        //Fill letter collection
        GridView gridview = (GridView) findViewById(R.id.lettergrid);
        gridview.setAdapter(letterCollectionAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                removeLetterFromCollection(position);
            }
        });
    }


    public void checkAndSubmitWord(View view){

        Log.e("AdventureMode","checkAndSubmitWord called");

        WeakHashMap<String, Integer> submissionLetterCount = new WeakHashMap<String, Integer>();
        EditText word_submission_field = (EditText) findViewById(R.id.word_submission);
        String word_submission = word_submission_field.getText().toString();



        for(int i = 0; i < word_submission.length(); i++)
        {
            String c = Character.toString(word_submission.charAt(i));

            Integer count = submissionLetterCount.get(c);
            if (count != null) {
                submissionLetterCount.put(c, count + 1);
            } else {
                submissionLetterCount.put(c, 1);
            }
        }

        if (checkEnoughLetters(word_submission, submissionLetterCount) && inGrabbleDictionary(word_submission)) {
            removeLettersFromCollection(submissionLetterCount);
            awardPoints(submissionLetterCount);
        }

    }

    private Boolean checkEnoughLetters(String word_submission, WeakHashMap<String, Integer> submissionLetterCount) {

        for (WeakHashMap.Entry<String, Integer> entry : submissionLetterCount.entrySet()) {
            String letter = entry.getKey();
            Integer count = entry.getValue();

            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID };

            // Filter results WHERE "title" = 'My Title'
            String selection = GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER + " = ?";
            String[] selectionArgs = {(letter)};

            Cursor cursor = readDb.query(
                    GrabbleContract.AdventureModeCollection.TABLE_NAME,       // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    null                                      // don't sort order
            );

            if (cursor.getCount() < count) {
                Toast.makeText(getBaseContext(), "Not enough of the letter "+letter, Toast.LENGTH_SHORT).show();
                Log.e("AdventureMode","Not enough of the letter "+letter);
                return false;
            }

            cursor.close();

        }

        //otherwise
        Log.e("AdventureMode","Yes, there is enough letters");
        return true;
    }

    private Boolean inGrabbleDictionary(String word_submission) {

        String[] projection = {
                GrabbleContract.GrabbleDictionary.COLUMN_NAME_WORD_INDEX,
                GrabbleContract.GrabbleDictionary.COLUMN_NAME_WORD,
        };

        // Search for lowercase word in dictionary
        String selection = GrabbleContract.GrabbleDictionary.COLUMN_NAME_WORD_INDEX + " = ?";
        String[] selectionArgs = {( word_submission )};

        Cursor cursor = readDb.query(
                GrabbleContract.GrabbleDictionary.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );

        if (cursor.getCount() > 0) {
            Log.e("AdventureMode","Yes, the word is in the dictionary");
            return true;

        } else {
            Toast.makeText(getBaseContext(), "That word is not in the dictionary", Toast.LENGTH_SHORT).show();
            Log.e("AdventureMode","No, the word is not in the dictionary");
            return false;
        }
    }

    private void removeLettersFromCollection(WeakHashMap<String, Integer> submissionLetterCount){


        for (WeakHashMap.Entry<String, Integer> entry : submissionLetterCount.entrySet()) {
            String letter = entry.getKey();
            Integer count = entry.getValue();

            // Filter results WHERE "title" = 'My Title'
            String selection = GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID + " IN (" + makeListPlaceholders(count) +")";
            String[] selectionArgs = getLetterDbIds(letter,count);

            int rowsDeleted = writeDb.delete(
                    GrabbleContract.AdventureModeCollection.TABLE_NAME,       // The table to query
                    selection,                                // The columns for the WHERE clause
                    selectionArgs                             // The values for the WHERE clause
            );

            Log.e("AdventureMode","deleted " + rowsDeleted + " of letter " + letter);

        }

        letterCollectionAdapter.notifyDataSetChanged();

    }

    String makeListPlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    private void awardPoints(WeakHashMap<String, Integer> submissionLetterCount) {
        Integer score = 0;

        for (WeakHashMap.Entry<String, Integer> entry : submissionLetterCount.entrySet()) {
            String letter = entry.getKey();
            Integer count = entry.getValue();

            score += (letterScores.get(letter) * count);
        }

        Toast.makeText(getBaseContext(), score + " points awarded", Toast.LENGTH_SHORT).show();
        Log.e("AdventureMode",score + " points awarded");

        updateScore(score);
    }

    private void updateScore(Integer score){

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.Scores.COLUMN_NAME_VALUE };

        // Filter results WHERE "title" = 'My Title'
        String selection = GrabbleContract.Markers.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {(GrabbleContract.SCORE_ADVENTURE_MODE_HIGH_CURRENT)};

        Cursor cursor = readDb.query(
                GrabbleContract.Scores.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort order
        );

        Integer oldTotal = null;
        while (cursor.moveToNext()) {
            oldTotal = cursor.getInt(cursor.getColumnIndexOrThrow(GrabbleContract.Scores.COLUMN_NAME_VALUE));
        }
        cursor.close();

        Integer newTotal = oldTotal + score;

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(GrabbleContract.Scores.COLUMN_NAME_VALUE, newTotal);

        readDb.update(
                GrabbleContract.Scores.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        updateScoreBoard(newTotal);

        Log.e("AdventureMode","Old Total" + oldTotal);
        Log.e("AdventureMode","New Total" + newTotal);
    }

    private Integer fetchHighScore() {
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.Scores.COLUMN_NAME_VALUE };

        // Filter results WHERE "title" = 'My Title'
        String selection = GrabbleContract.Markers.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {(GrabbleContract.SCORE_ADVENTURE_MODE_HIGH_CURRENT)};

        Cursor cursor = readDb.query(
                GrabbleContract.Scores.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort order
        );

        Integer score = null;
        while (cursor.moveToNext()) {
            score = cursor.getInt(cursor.getColumnIndexOrThrow(GrabbleContract.Scores.COLUMN_NAME_VALUE));
        }
        return score;
    }

    private void updateScoreBoard(Integer score){

        TextView scoreBoard = (TextView) findViewById(R.id.scoreBoard);
        scoreBoard.setText("Score: "+score);

    }

    public String[] getLetterDbIds(String letter,Integer count) {
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID };

        // Filter results WHERE "title" = 'My Title'
        String selection =  GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER + " = ?";
        String[] selectionArgs = {(letter)};


        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,                                     // don't sort order
                "0,"+count
        );



        String[] ids = new String[cursor.getCount()];

        int i=-1;
        while (cursor.moveToNext()) {
            i++;
            ids[i] = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID));
        }
        cursor.close();
        return ids;
    }


    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
