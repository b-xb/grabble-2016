package com.example.ben.newtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;

import java.util.Calendar;

public class MainMenuActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    /** Called when the user clicks the Adventure Mode button */
    public void launchAdventureMode(View view) {
        Intent intent = new Intent(this, AdventureModeActivity.class);
        startActivity(intent);
    }

    /** Called when the user clicks the Adventure Mode button */
    public void launchTimerMode(View view) {
        Toast.makeText(getBaseContext(), "Coming Soon!", Toast.LENGTH_LONG).show();
    }

    /** Called when the user clicks the Adventure Mode button */
    public void launchChallengeMode(View view) {
        Toast.makeText(getBaseContext(), "Coming Soon!", Toast.LENGTH_LONG).show();
    }

    /** Called when the user clicks the Adventure Mode button */
    public void launchAchievements(View view) {
        Toast.makeText(getBaseContext(), "Coming Soon!", Toast.LENGTH_LONG).show();
    }

    /** Called when the user clicks the Adventure Mode button */
    public void launchHelp(View view) {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

}
