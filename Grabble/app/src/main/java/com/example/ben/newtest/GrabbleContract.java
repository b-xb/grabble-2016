package com.example.ben.newtest;

import android.content.SharedPreferences;
import android.provider.BaseColumns;

public class GrabbleContract {

    public static final String SHARED_PREFERENCES = "shared_preferences";
    public static final String CURRENT_MAP_DATE = "map_date";

    public static final String SUNDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/sunday.kml";
    public static final String MONDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/monday.kml";
    public static final String TUESDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/tuesday.kml";
    public static final String WEDNESDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/wednesday.kml";
    public static final String THURSDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/thursday.kml";
    public static final String FRIDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/friday.kml";
    public static final String SATURDAY_MAP_URI = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/saturday.kml";


    public static final String[] MAP_URIS = {
            null,
            SUNDAY_MAP_URI,
            MONDAY_MAP_URI,
            TUESDAY_MAP_URI,
            WEDNESDAY_MAP_URI,
            THURSDAY_MAP_URI,
            FRIDAY_MAP_URI,
            SATURDAY_MAP_URI

    };


    public static final Integer MARKER_UP_FOR_GRABS = 0;
    public static final Integer MARKER_GRABBED = 1;

    /* Inner class that defines the table contents */
    public static class Markers implements BaseColumns {
        public static final String TABLE_NAME = "markers";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_LETTER = "letter";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_ADVENTURE_MODE_STATE = "adventure_state";
        public static final String COLUMN_NAME_CHALLENGE_MODE_STATE = "challenge_state";
        public static final String COLUMN_NAME_TIMER_MODE_STATE = "timer_state";
    }

    /* Inner class that defines the table contents */
    public static class AdventureModeCollection implements BaseColumns {
        public static final String TABLE_NAME = "adventure_mode_collection";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_LETTER = "letter";
    }

    /* Inner class that defines the table contents */
    public static class AdventureModeCollectionAlt implements BaseColumns {
        public static final String TABLE_NAME = "adventure_mode_collection_alt";
        public static final String COLUMN_NAME_LETTER = "letter";
        public static final String COLUMN_NAME_COUNT = "count";
    }

    /* Inner class that defines the table contents */
    public static class GrabbleDictionary implements BaseColumns {
        public static final String TABLE_NAME = "dictionary";
        public static final String COLUMN_NAME_WORD_INDEX = "word_index";
        public static final String COLUMN_NAME_WORD = "word";
    }

    public static final String SCORE_ADVENTURE_MODE_HIGH_CURRENT = "ADVENTURE_MODE_HIGH_CURRENT";

    /* Inner class that defines the table contents */
    public static class Scores implements BaseColumns {
        public static final String TABLE_NAME = "scores";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_VALUE = "value";
    }

}
