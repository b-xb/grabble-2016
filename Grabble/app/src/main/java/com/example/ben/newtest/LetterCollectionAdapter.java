package com.example.ben.newtest;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.WeakHashMap;

public class LetterCollectionAdapter extends BaseAdapter {
    private Context mContext;
    private WeakHashMap<String, Integer> letterTiles;
    private SQLiteDatabase readDb;

    public LetterCollectionAdapter(Context c, WeakHashMap<String, Integer> tiles, SQLiteDatabase db) {
        mContext = c;
        letterTiles = tiles;
        readDb = db;
    }

    public int getCount() {
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER };

        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,
                null,
                null,
                null,
                null
        );

        return cursor.getCount();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(getTile(position));
        return imageView;
    }

    // references to our images
    private Integer getTile(int position) {

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER };


        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,                                     // don't sort order
                position+",1"
        );

        String letterValue = null;
        while (cursor.moveToNext()) {
            letterValue = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER));
        }
        cursor.close();

        Log.e("LetterCollectionAd","Tile loaded: "+letterValue+" position: "+position);

        return letterTiles.get(letterValue);
    }


    public Object getLetter(int position) {

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER };


        Cursor cursor = readDb.query(
                GrabbleContract.Markers.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,                                     // don't sort order
                position+",1"
        );

        String letterValue = null;
        while (cursor.moveToNext()) {
            letterValue = cursor.getString(cursor.getColumnIndexOrThrow(GrabbleContract.Markers.COLUMN_NAME_LETTER));
        }
        cursor.close();

        return letterValue;
    }

    public long getLetterDbId(int position) {
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = { GrabbleContract.AdventureModeCollection.COLUMN_NAME_LETTER };


        Cursor cursor = readDb.query(
                GrabbleContract.AdventureModeCollection.TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,                                     // don't sort order
                position+",1"
        );

        Long id = null;
        while (cursor.moveToNext()) {
            id = cursor.getLong(cursor.getColumnIndexOrThrow(GrabbleContract.AdventureModeCollection.COLUMN_NAME_ID));
        }
        cursor.close();

        return id;
    }

}